# Giraffe

This board offers access to the SBU1 and SBU2 pins from the USB-C pinout at the convenience of passing through all the other signals to the host from the device. The signals on SBU pins are vendor specific and typically include UART if connected.

The dongle also correctly connects the Rx and Tx pins to the SBU1 and SBU2 pins based on the orientation detected on the CC1 and CC2 pins.

Since the voltage levels on the SBU1 and SBU2 can vary there's a voltage selection header where you can select with a jumper the 3.3V or 1.8V for the voltage level shifter on the serial port.

Other voltages beside 1.8V could be supported if the LDO is swapped for compatible one with the target voltage.

The board features solder bridges for connection of the SBU1/2 and CC1/2 signals as well as placeholders for pull-down resistors for CC1/CC2 in case the device would be missing them.

As the UART<->USB converter IC was swapped for MCU it's now possible to also monitor the USB PD communication on the CC1/2 signals.

Last feature is shorting the USB 'D+' signal that is typically used during booting (immediately after connecting power) to signal device processor to enter boot mode - Qualcomm EDL mode. This can either be done manually by pressing the boot button or triggered through the MCU.

## 3D View
![3D view Front](images/Giraffe_3D_Front_R2.png)
![3D view Back](images/Giraffe_3D_Back_R2.png)

## Credits
The idea is based on the original project from [Google](https://github.com/google/usb-cereal).

I'd like to thank very much my friends Michal Kočí and Jiří Petrovský for ideas regarding routing signals and for the final review.

## Changelog
### Revision 2 changes

Instead of using dedicated USB<->UART converter the IC is replaced with RISC-V MCU WCH32X035.

It's programmable from USB and beside the UART conversion there's now native support for USB PD communication and interception.

Additionally proper LDO is used for 1.8V and the switch allows selectable voltage. In fact the IC can be replaced for different voltage as the footprint is compatible.

Proper USB connectors are used. For the Debug device connector only 16P connector is used and for the female part the connector is strongly attached to the board to prevent any bending.

Additionally the solder bridges on signals CC1/2 and SBU1/2 allow to route the signals by closing/leaving them open.

Fiducials are placed for PCBA and all components are placed just on top of the board.
